from django.urls import path

from reviews.views import list_reviews, create_reviews

urlpatterns = [
    path("", list_reviews, name="reviews_list"),
    path("new/", create_review, name="create_review"),
]